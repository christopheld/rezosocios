<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/rezosocios?lang_cible=pt_br
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_activer_habillage_explication' => 'Substituir os títulos por ícones nas entradas (modelo, menu, componente). Estes são baseados na fonte Socicon.',
	'cfg_activer_habillage_label' => 'Roupagem', # RELIRE
	'cfg_activer_habillage_label_case' => 'Ativar a roupagem no site público', # RELIRE
	'cfg_apercu_habillage' => 'Prévia:',
	'cfg_titre_rezosocios' => 'Configuração do "Redes Sociais"',

	// E
	'erreur_url_utilisee' => 'O link do perfil de rede social já está sendo usado.',

	// I
	'icone_creation_rezosocio' => 'Criar um link de rede social',
	'icone_rezosocios' => 'Redes sociais',
	'icone_supprimer_rezosocio' => 'Excluir esta rede social',
	'info_langue' => 'Idioma do perfil',
	'info_modifier_rezosocio' => 'Alterar o link de rede social',
	'info_nom_compte' => 'Nome da conta',
	'info_nom_compte_explication' => 'Informe apenas o identificador da conta, e não o URL completo. Exemplo: para https://twitter.com/Spip, informe «Spip»',
	'info_recherche_rezosocio_zero' => 'Nenhum resultado nas redes sociais para a busca "@cherche_rezosocio@"',
	'info_retirer_rezosocio' => 'Excluir este link de rede social',
	'info_retirer_rezosocios' => 'Excluir todas as redes sociais',
	'info_type_rezosocios' => 'Tipo de rede social',
	'info_url_site' => 'Link do perfil',

	// L
	'label_activer_rezosocio_objets' => 'Ativar os links de redes sociais para os conteúdos:',
	'lien_ajouter_rezosocio' => 'Incluir este link de rede social',

	// M
	'menu_description' => 'Você pode ter um ou mais links na entrada do menu',
	'menu_id_rezosocio_label' => 'Um ou mais números separados por vírgulas.',
	'menu_titre' => 'Links para as redes sociais',

	// N
	'noisette_contenu_label' => 'Conteúdo',
	'noisette_id_rezosocio_label' => 'Seleção',
	'noisette_liaison_explication' => 'Pegar as redes ligadas a um conteúdo?',
	'noisette_liaison_label' => 'Ligação',
	'noisette_liaison_non' => 'Não',
	'noisette_liaison_objet_contexte' => 'O conteúdo do contexto',
	'noisette_liaison_objet_lie' => 'Um conteúdo escolhido manualmente',
	'noisette_objet_lie_explication' => 'Atalho de um conteúdo: article10, rubrique5 etc. Com o plugin Seletor Genérico, digite as primeiras letras de um título para obter sugestões.',
	'noisette_objet_lie_label' => 'Conteúdo ligado',
	'noisette_rezosocios_description' => 'Barra de links para as redes sociais, podendo ser filtrada de acordo com diversos critérios.',
	'noisette_rezosocios_nom' => 'Links para as redes sociais',

	// R
	'rezosocio' => 'Rede social',
	'rezosocio_ajouter' => 'Incluir um link de rede social',
	'rezosocio_aucun' => 'Nenhum link de rede social',
	'rezosocio_creer_associer' => 'Criar e vincular um link de rede social',
	'rezosocio_editer' => 'Alterar este link de rede social',
	'rezosocio_logo' => 'Logo do link de rede social',
	'rezosocio_un' => '1 link de rede social',
	'rezosocios' => 'Redes sociais',
	'rezosocios_nb' => '@nb@ links de redes sociais',

	// S
	'saisie_rezosocio_description' => 'Um ou mais links de redes sociais',
	'saisie_rezosocio_option_multiple_label' => 'Múltiplas escolhas',
	'saisie_rezosocio_option_multiple_label_case' => 'Várias opções possíveis',
	'saisie_rezosocio_option_type_rezo_label' => 'Tipo de rede',
	'saisie_rezosocio_titre' => 'Seleção de links de redes sociais',

	// T
	'texte_nouveau_rezosocio' => 'Novo link de rede social',
	'texte_rezosocio_statut' => 'Este link de rede social está:',
	'titre_ajouter_un_rezosocio' => 'Incluir um link de rede social',
	'titre_objets_lies_rezosocio' => 'Objetos vinculados a esta rede social',
	'titre_page_rezosocios_page' => 'Os links  de redes sociais',
	'titre_tweetsde' => 'Tweets de @@name@',
	'twitter_hashtag' => 'Twitter (hashtag)',

	// Y
	'youtube_channel' => 'Youtube (canal)',
	'youtube_user' => 'Youtube (conta de usuário)'
);
